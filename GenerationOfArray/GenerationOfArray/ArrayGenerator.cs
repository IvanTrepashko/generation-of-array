﻿using System;

namespace GenerationOfArray
{
    /// <summary>
    /// Represents a class for generating different kinds of arrays.
    /// </summary>
    public static class ArrayGenerator
    {
        /// <summary>
        /// Creates an array of int numbers in a specified range sorted ascending.
        /// </summary>
        /// <param name="startRange">Start of the range.</param>
        /// <param name="endRange">End of the range.</param>
        /// <returns>Ascending array of elements in a specified range.</returns>
        public static int[] CreateAscendingArray(int startRange, int endRange)
        {
            if (startRange > endRange)
            {
                throw new ArgumentException("Start of range can't be bigger than end of range.");
            }

            int length = Math.Abs(endRange - startRange);
            int[] array = new int[length + 1];

            for (int i = 0; i <= length; i++)
            {
                array[i] = startRange;
                startRange++;
            }

            return array;
        }

        /// <summary>
        /// Creates an array of int numbers in a specified range sorted descending.
        /// </summary>
        /// <param name="startRange">Start of the range.</param>
        /// <param name="endRange">End of the range.</param>
        /// <returns>Descending array of elements in a specified range.</returns>
        public static int[] CreateDescendingArray(int startRange, int endRange)
        {
            if (startRange < endRange)
            {
                throw new ArgumentException("Start of range can't be less than end of range.");
            }

            int length = Math.Abs(endRange - startRange);
            int[] array = new int[length + 1];

            for (int i = 0; i <= length; i++)
            {
                array[i] = startRange;
                startRange--;
            }

            return array;
        }

        /// <summary>
        /// Returns an array of random numbers containing specified digit.
        /// </summary>
        /// <param name="digit">Specified digit.</param>
        /// <param name="length">Array length.</param>
        /// <returns>An array of random numbers containing specified digit.</returns>
        public static int[] CreateArrayFromDigit(int digit, int length)
        {
            if (digit < 0 || digit > 9)
            {
                throw new ArgumentOutOfRangeException(nameof(digit), "Digit can't be less than 0 or more than 9");
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Array length can't be less than 0.");
            }

            int[] array = new int[length];

            Random rnd = new Random();

            for (int i = 0; i < length; i++)
            {
                int x = rnd.Next(1, 10);
                int number = digit;

                for (int j = 0; j < x; j++)
                {
                    number = (number * 10) + digit;
                }

                array[i] = number;
            }

            return array;
        }
    }
}
