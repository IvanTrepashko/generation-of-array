﻿using System;

namespace GenerationOfArray
{
    /// <summary>
    /// Represents a class for mixing elements of an array.
    /// </summary>
    public static class ArrayMixer
    {
        /// <summary>
        /// Mixes elements of source array using Fisher–Yates shuffle.
        /// </summary>
        /// <param name="array">Source array.</param>
        public static void MixUsingFisherYatesShuffle(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Random rnd = new Random();

            for (int i = 0; i < array.Length; i++)
            {
                int index = rnd.Next(0, i);
                
                Swap(ref array[i], ref array[index]);
            }
        }

        /// <summary>
        /// Mixes elements of source array using DateTime.
        /// </summary>
        /// <param name="array">Source array.</param>
        public static void MixUsingDateTime(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (int i = 0; i < array.Length; i++)
            {
                int index = (DateTime.Now.Millisecond % array.Length) - 1;

                Swap(ref array[i], ref array[index]);
            }
        }

        /// <summary>
        /// Mixes elements of source array using Sattolo shuffle.
        /// </summary>
        /// <param name="array">Source array.</param>
        public static void MixUsingSattoloShuffle(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Random rnd = new Random();
            int n = array.Length;

            while (n > 1)
            {
                n -= 1;
                int j = rnd.Next(n);
                Swap(ref array[j], ref array[n]);
            }
        }

        private static void Swap(ref int x, ref int y)
        {
            int tmp = x;
            x = y;
            y = tmp;
        }
    }
}
