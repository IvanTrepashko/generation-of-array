using NUnit.Framework;
using System;
using System.Linq;

namespace GenerationOfArray.Tests
{
    [TestFixture]
    public class ArrayGeneratorTests
    {
        [TestCase(1,4)]
        [TestCase(0,10)]
        [TestCase(-10,3)]
        [TestCase(0,0)]
        [TestCase(-20,-3)]
        public void GenerateAscendingArray_ParametersAreValid_ReturnsArray(int startRange, int endRange)
        {
            int[] expected = Enumerable.Range(startRange, endRange - startRange + 1).ToArray();
            
            int[] actual = ArrayGenerator.CreateAscendingArray(startRange, endRange);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(10,1)]
        [TestCase(-4,-10)]
        [TestCase(1,-4)]
        public void GenerateAscendingArray_ParametersAreInvalid_ThrowsArgumentException(int startRange, int endRange)
            => Assert.Throws<ArgumentException>(() => ArrayGenerator.CreateAscendingArray(startRange, endRange));


        [TestCase(4, 1)]
        [TestCase(10, 0)]
        [TestCase(3, -10)]
        [TestCase(0, 0)]
        [TestCase(-3, -20)]
        public void GenerateDescendingArray_ParametersAreValid_ReturnsArray(int startRange, int endRange)
        {
            int[] expected = Enumerable.Range(endRange, startRange - endRange + 1).Reverse().ToArray();

            int[] actual = ArrayGenerator.CreateDescendingArray(startRange, endRange);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, 10)]
        [TestCase(-10, -4)]
        [TestCase(-4, 1)]
        public void GenerateDescendingArray_ParametersAreInvalid_ThrowsArgumentException(int startRange, int endRange)
           => Assert.Throws<ArgumentException>(() => ArrayGenerator.CreateDescendingArray(startRange, endRange));

        [TestCase(13, 3)]
        [TestCase(-3, 1)]
        [TestCase(4, -5)]
        public void GenerateArrayFromDigit_ParametersAreInvalid_ThrowsArgumentOutOfRangeException(int digit, int length) =>
            Assert.Throws<ArgumentOutOfRangeException>(() => ArrayGenerator.CreateArrayFromDigit(digit, length));
    }
}