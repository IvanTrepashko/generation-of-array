﻿using NUnit.Framework;
using System;

namespace GenerationOfArray.Tests
{
    [TestFixture]
    class ArrayMixerTests
    {
        [Test]
        public void MixUsingFisherYatesShuffle_SourceArrayIsNull_ThrowsArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => ArrayMixer.MixUsingFisherYatesShuffle(null));

        [Test]
        public void MixUsingDateTime_SourceArrayIsNull_ThrowsArgumentNullException() 
            => Assert.Throws<ArgumentNullException>(() => ArrayMixer.MixUsingDateTime(null));

        [Test]
        public void MixUsingSattoloShuffle_SourceArrayIsNull_ThrowsArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => ArrayMixer.MixUsingSattoloShuffle(null));
    }
}
